import re

COMMAND_ERROR = ""


def help(s: str) -> str:
    if s[:4] == "help":
        return ""
    return COMMAND_ERROR


def info(s: str) -> str:
    if s[:4] == "info" or s[:2] == "\i":
        return ""
    return COMMAND_ERROR


def make_env(title):
    """

    """
    pass


def parse_command(s: str) -> str:
    if not s:
        return COMMAND_ERROR
    if re.search("(^mkenv) (.*)", s):
        v = re.search("(^mkenv) (.*)", s).groups()[1].strip()

    elif re.search("(^mkboard) (.*)", s):
        v = re.search("(^mkboard) (.*)", s).groups()[1].strip()

    elif re.search("(^mkclmns) (.*)", s):
        v = re.search("(^mkclmns) (.*)", s).groups()[1].strip()

    elif re.search("(^mk) (.*)", s):
        v = re.search("(^mk) (.*)", s).groups()[1].strip()

    elif re.search("(^cd) (.*)", s):
        v = re.search("(^cd) (.*)", s).groups()[1].strip()

    elif re.search("(^ls) (.*)", s):
        v = re.search("(^ls) (.*)", s).groups()[1].strip()

    return COMMAND_ERROR

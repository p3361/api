from uuid import uuid4
import datetime as dt
from sqlalchemy import (
    Column, ForeignKey,
    Integer, String, DateTime, Boolean, Text,
    BIGINT, ARRAY
)
from sqlalchemy import create_engine
from sqlalchemy.dialects.postgresql import UUID, JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, scoped_session

from app.config import SQLALCHEMY_DATABASE_URL

engine = create_engine(SQLALCHEMY_DATABASE_URL)
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
session = Session()

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, index=True)
    name = Column(String(128), index=True)
    email = Column(String(128), index=True)
    password_hash = Column(String(256), nullable=False)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    environments = relationship("UserEnvironment", back_populates="user")
    boards = relationship("UserEnvironment", back_populates="user")
    tasks = relationship("UserTask", back_populates="user")
    logs = relationship("UserLog", back_populates="user")
    state = relationship("UserState", back_populates="user")


class Environment(Base):
    __tablename__ = "environments"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    title = Column(String(256), nullable=False)
    description = Column(Text)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    users = relationship("UserEnvironment", back_populates="environment")
    boards = relationship("Board", back_populates="environment")


class UserEnvironment(Base):
    __tablename__ = "user_environments"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user_id = Column(UUID(as_uuid=True), ForeignKey("users.id"), index=True)
    environment_id = Column(BIGINT, ForeignKey("environments.id"), index=True)
    role = Column(String(32))
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    user = relationship("User", back_populates="environments")
    environment = relationship("Environment", back_populates="users")


class Board(Base):
    __tablename__ = "boards"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    environment_id = Column(BIGINT, ForeignKey("environments.id"), index=True)
    title = Column(String(256), nullable=False)
    description = Column(Text)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    environment = relationship("Environment", back_populates="boards")
    users = relationship("UserBoard", back_populates="board")
    columns = relationship("BoardColumn", back_populates="board")


class UserBoard(Base):
    __tablename__ = "user_boards"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user_id = Column(UUID(as_uuid=True), ForeignKey("users.id"), index=True)
    board_id = Column(BIGINT, ForeignKey("boards.id"), index=True)
    role = Column(String(32))
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    user = relationship("User", back_populates="environments")
    board = relationship("Board", back_populates="users")


class BoardColumn(Base):
    __tablename__ = "board_columns"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    board_id = Column(BIGINT, ForeignKey("boards.id"), index=True)
    title = Column(String(256), nullable=False)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    board = relationship("Board", back_populates="columns")


class Task(Base):
    __tablename__ = "tasks"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    title = Column(String(256), nullable=False)
    column_id = Column(BIGINT, ForeignKey("board_columns.id"), index=True)
    description = Column(Text)
    status = Column(String(32))
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    users = relationship("UserTask", back_populates="task")


class UserTask(Base):
    __tablename__ = "user_tasks"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user_id = Column(UUID(as_uuid=True), ForeignKey("users.id"), index=True)
    task_id = Column(BIGINT, ForeignKey("tasks.id"), index=True)
    role = Column(String(32))
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    user = relationship("User", back_populates="tasks")
    task = relationship("Task", back_populates="users")


class UserLog(Base):
    __tablename__ = "user_logs"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user_id = Column(UUID(as_uuid=True), ForeignKey("users.id"), index=True)
    command = Column(String(512))
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    user = relationship("User", back_populates="tasks")


class UserState(Base):
    __tablename__ = "user_state"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user_id = Column(UUID(as_uuid=True), ForeignKey("users.id"), index=True)
    state = Column(String(64))
    updated_at = Column(DateTime, default=dt.datetime.utcnow)

    user = relationship("User", back_populates="state")

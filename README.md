# Terminal Task Tracker
Terminal interface for manage tasks.

### Commands
> `help` - help 
>
> `\i` - info about my user
> 
> `info` - info about my user
>
> `mkenv some_title` - create new environment
> 
> `mkboard some_title` - create new board
> 
> `mkclmns column1, column2` - add columns
> 
> `mk some_title` - create new obj
> 
> `cd some_title` - move to
> 
> `cd` - move to prev
> 
> `cd ../` - move to prev
> 
> `cd ..` - move prev
> 
> `ls` - view all objects
> 
> `ls -a` - view all objects
> 
> `ls -m` - view only my objects
> 
> `ls -u ...` - view user objects
> 
> `rm some_title` - remove obj
> 
> `rm *` - remove all